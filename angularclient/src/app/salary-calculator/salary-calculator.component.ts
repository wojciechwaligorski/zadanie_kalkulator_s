import {Component, OnInit} from '@angular/core';
import {CountryService} from "../country/country.service";
import {Country} from "../country/country";
import {DailySalary} from "../daily-salary/daily-salary";
import {Salary} from "../salary/salary";
import {SalaryService} from "../salary/salary.service";

@Component({
  selector: 'app-salary-calculator',
  templateUrl: './salary-calculator.component.html',
  styleUrls: ['./salary-calculator.component.sass']
})
export class SalaryCalculatorComponent implements OnInit {

  dailySalaryModel: DailySalary = new DailySalary(0, 'PL');
  salaryModel: Salary = new Salary(0);
  countries: Country[];
  countingError: boolean = false;
  currencyCode: string = 'PLN';

  constructor(private countryService: CountryService,
              private salaryService: SalaryService) {
  }

  ngOnInit() {
    this.countryService.findAllHttp().subscribe(data => {
      this.countries = data;
      this.dailySalaryModel.countryCode = this.countries[0].countryCode;
    });
  }

  onCountSalary(): void {
    this.salaryService.countSalaryHttp(this.dailySalaryModel).subscribe(
      (data) => {
        this.salaryModel = data;
      },
      (error: Error) => {
        this.countingError = true;
      });
  }

  onCountryChange(countryCode) {
    for (let country of  this.countries) {
      if (country.countryCode == countryCode) {
        this.currencyCode = country.currencyCode;
        break;
      }
    }
  }

}
