import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Salary} from "./salary";

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  private readonly countSalaryUrl: string;

  constructor(private http: HttpClient) {
    this.countSalaryUrl = 'http://localhost:8080/salary/calculate-polish';
  }

  public countSalaryHttp(dailySalaryModel) {
    console.log("dailySalaryModel send by POST: " + dailySalaryModel.toString())
    return this.http.post<Salary>(this.countSalaryUrl, dailySalaryModel);
  }
}
