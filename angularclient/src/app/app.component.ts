import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Kalkulator wynagrodzenia';
  description = 'Kalkulator oblicza miesięczną stawkę netto w wybranym kraju na podstawie wynagrodzenia dziennego brutto';
}
