export class Country {
  constructor(
    public name: string,
    public countryCode: string,
    public currencyCode: string,
    ) {
  }
}
