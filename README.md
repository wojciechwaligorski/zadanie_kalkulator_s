## Salary calculator

The application calculates monthly salary net in polish currency, taking daily salary gross in choosen currency.


## Getting started

**Backend**

In directory **salary_calculator**:

	mvn clean install
	mvn spring-boot:run
	
	
**Frontend**

In directory  **angularclient**:

	npm install
	ng serve


## Prerequest

Installed:

	Java 1.8 
	Maven 	
	Angular CLI


## Author

Wojciech Waligórski


## License

This project is licensed under the MIT License
