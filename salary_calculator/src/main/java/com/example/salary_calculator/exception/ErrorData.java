package com.example.salary_calculator.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorData {

    private String status;
    private Error error;

    public ErrorData(String status, String message, String details) {
        this.status = status;
        this.error = new Error(message, details);
    }
}
