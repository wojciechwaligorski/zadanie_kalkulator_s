package com.example.salary_calculator.exception;

import com.example.salary_calculator.bnp.NullRateException;
import com.example.salary_calculator.country.WrongCountryCodeException;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {

    public static final String ERROR_RESPONSE_FROM_REMOTE_SERVICE = "Error response from remote server";
    public static final String ERROR_CONNECTION_WITH_REMOTE_SERVER = "Error connection with remote server";
    public static final String INTERNAL_ERROR = "Internal server error";
    public static final String ERROR_REQUEST = "Error request";

    @ExceptionHandler(WrongCountryCodeException.class)
    public ResponseEntity<ErrorData> handleHttpClientException(WrongCountryCodeException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorData> handleHttpClientException(HttpMessageNotReadableException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ErrorData> handleHttpClientException(NullPointerException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_REQUEST);
    }

    @ExceptionHandler(NullRateException.class)
    public ResponseEntity<ErrorData> handleHttpClientException(NullRateException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<ErrorData> handleHttpClientException(HttpClientErrorException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }

    @ExceptionHandler(value = {ResourceAccessException.class})
    public ResponseEntity<ErrorData> handleResourceAccessException(ResourceAccessException exception) {
        return prepareResponseEntity(exception, HttpStatus.REQUEST_TIMEOUT, ERROR_CONNECTION_WITH_REMOTE_SERVER);
    }

    @ExceptionHandler(value = {NoHandlerFoundException.class})
    public ResponseEntity<ErrorData> handleNoHandleFoundException(NoHandlerFoundException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_REQUEST);
    }

    @ExceptionHandler(value = {RestClientException.class})
    public ResponseEntity<ErrorData> handleRestClientException(NestedRuntimeException exception) {
        return prepareResponseEntity(exception, HttpStatus.BAD_REQUEST, ERROR_RESPONSE_FROM_REMOTE_SERVICE);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ErrorData> handleOtherExceptions(Exception exception) {
        return prepareResponseEntity(exception, HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_ERROR);
    }

    private ResponseEntity<ErrorData> prepareResponseEntity(Exception exception, HttpStatus httpStatus, String message) {
        ErrorData errorData = new ErrorData(httpStatus.toString(), message, exception.getMessage());
        return new ResponseEntity<>(errorData, httpStatus);
    }
}
