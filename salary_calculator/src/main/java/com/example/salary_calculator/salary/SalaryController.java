package com.example.salary_calculator.salary;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/salary")
public class SalaryController {

    private final SalaryService salaryService;

    @Autowired
    public SalaryController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    @PostMapping("/calculate-polish")
    public ResponseEntity<SalaryDTO> calculatePolishSalary(@RequestBody @NonNull DailySalary dailySalary)
            throws Exception {
        SalaryDTO salaryDTO = salaryService.calculateMonthlyNet(dailySalary);
        return new ResponseEntity<>(salaryDTO, HttpStatus.OK);
    }

}
