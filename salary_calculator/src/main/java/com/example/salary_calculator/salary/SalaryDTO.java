package com.example.salary_calculator.salary;

import com.example.salary_calculator.utils.BigDecimalSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class SalaryDTO {

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal monthlyNet;
}
