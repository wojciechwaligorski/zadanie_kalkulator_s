package com.example.salary_calculator.country;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country {

    private String name;
    private String countryCode;
    private String currencyCode;
    private BigDecimal fixedFee;
    private BigDecimal tax;
    private BigDecimal vat;

}
