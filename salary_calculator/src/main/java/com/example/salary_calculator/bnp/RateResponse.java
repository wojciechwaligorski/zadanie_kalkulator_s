package com.example.salary_calculator.bnp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateResponse {

    private String no;
    private Date effectiveDate;
    private BigDecimal mid;
}
