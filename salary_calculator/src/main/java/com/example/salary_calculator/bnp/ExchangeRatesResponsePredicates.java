package com.example.salary_calculator.bnp;

import java.util.function.Predicate;

public class ExchangeRatesResponsePredicates {

    public static Predicate<ExchangeRatesResponse> isNullOrEmpty() {
        return response -> response == null || response.getRates() == null || response.getRates().length == 0;
    }
}
