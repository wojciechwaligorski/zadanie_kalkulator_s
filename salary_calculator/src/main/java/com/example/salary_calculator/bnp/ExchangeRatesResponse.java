package com.example.salary_calculator.bnp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRatesResponse {

    private String table;
    private String no;
    private String effectiveDate;
    private RateResponse[] rates;

}
