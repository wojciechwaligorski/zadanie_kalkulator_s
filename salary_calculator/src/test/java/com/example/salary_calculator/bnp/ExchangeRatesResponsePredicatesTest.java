package com.example.salary_calculator.bnp;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ExchangeRatesResponsePredicatesTest {


    @Test
    public void isNullOrEmptyNullTrue() {
        boolean test = ExchangeRatesResponsePredicates.isNullOrEmpty().test(null);

        assertThat(test).isTrue();
    }

    @Test
    public void isNullOrEmptyEmptyTrue() {
        ExchangeRatesResponse exchangeRatesResponse = new ExchangeRatesResponse();
        exchangeRatesResponse.setNo("test");
        exchangeRatesResponse.setTable("test");
        exchangeRatesResponse.setEffectiveDate("test");
        exchangeRatesResponse.setRates(new RateResponse[0]);
        boolean test = ExchangeRatesResponsePredicates.isNullOrEmpty().test(exchangeRatesResponse);

        assertThat(test).isTrue();
    }

    @Test
    public void isNullOrEmptyFalse() {
        ExchangeRatesResponse exchangeRatesResponse = new ExchangeRatesResponse();
        exchangeRatesResponse.setRates(new RateResponse[1]);
        boolean test = ExchangeRatesResponsePredicates.isNullOrEmpty().test(exchangeRatesResponse);

        assertThat(test).isFalse();
    }
}
