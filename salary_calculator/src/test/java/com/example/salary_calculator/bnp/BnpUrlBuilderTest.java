package com.example.salary_calculator.bnp;

import org.junit.Test;

import static com.example.salary_calculator.bnp.BnpUrlBuilder.API_EXCHANGERATES_RATES_A;
import static com.example.salary_calculator.bnp.BnpUrlBuilder.BASE_BNP_URL;
import static org.assertj.core.api.Assertions.assertThat;

public class BnpUrlBuilderTest {

    @Test
    public void buildNull() {
        BnpUrlBuilder builder = new BnpUrlBuilder();
        String build = builder.build(null);
        assertThat(build).isNull();
    }

    @Test
    public void buildSuccess() {
        BnpUrlBuilder builder = new BnpUrlBuilder();
        String build = builder.build("PLN");
        assertThat(build).isNotNull();
        assertThat(build).isEqualTo(BASE_BNP_URL + API_EXCHANGERATES_RATES_A + "PLN");
    }

}
