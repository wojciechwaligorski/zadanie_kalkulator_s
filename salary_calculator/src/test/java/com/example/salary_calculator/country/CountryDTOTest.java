package com.example.salary_calculator.country;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class CountryDTOTest {

    @Test
    public void constructEmpty() {
        CountryDTO countryDTO = new CountryDTO(null);
        assertThat(countryDTO).isNotNull();
    }

    @Test
    public void constructSuccess() {
        Country country = new Country("Polska", "PL", "PLN", null, null, null);
        CountryDTO countryDTO = new CountryDTO(country);

        assertThat(countryDTO).isNotNull();
        assertThat(countryDTO.getName()).isEqualTo(country.getName());
        assertThat(countryDTO.getCountryCode()).isEqualTo(country.getCountryCode());
        assertThat(countryDTO.getCurrencyCode()).isEqualTo(country.getCurrencyCode());
    }
}
