package com.example.salary_calculator.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileNotFoundException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
public class ResourceUtilsTest {

    @Spy
    ResourceUtils resourceUtils;

    @Test(expected = NullPointerException.class)
    public void getFileResourceFileNull() throws FileNotFoundException {
        resourceUtils.getFileResourceFile(null);
    }

    @Test(expected = FileNotFoundException.class)
    public void getFileResourceFileNotFound() throws FileNotFoundException {
        resourceUtils.getFileResourceFile("test");
    }

    @Test
    public void getFileResourceFileSuccess() throws FileNotFoundException {
        File file = resourceUtils.getFileResourceFile("data/countries.json");
        assertThat(file).isNotNull();
        assertThat(file.exists()).isTrue();
    }
}
